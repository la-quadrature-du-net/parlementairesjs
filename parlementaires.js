"use strict";

function ParlementairesJS(options) {
	return Object.create({
		options: options,
		display: display
	});
};

function display(targetId) {
	if (!this.options) throw new Error("Options are missing");
	if (!this.options.dataset) throw new Error("Empty data set.");

	if (!targetId) throw new Error("HTML ID missing.");

	var target = document.getElementById(targetId);

	if (!target) throw new Error("Cannot find target with id " + targetId + ".");

	// Initiate vars
	var mps = [];
	var groups = [];
	var hiddenGroups = (!this.options.hiddenGroups) ? [] : this.options.hiddenGroups;
	var counties = [];
	var selected = [];
	var group = 0;
	var county = 0;
	var currentMp = 0;

	// HTML vars
	var parlementairesjsMpTotal,
		parlementairesjsSelectGroupDefaultOption,
		parlementairesjsSelectGroup,
		parlementairesjsSelectCountyDefaultOption,
		parlementairesjsSelectCounty,
		parlementairesjsIntrophone,
		parlementairesjsIntrophoneWrapper,
		parlementairesjsMpPhoto,
		parlementairesjsMpPhotoWrapper,
		parlementairesjsMpFirstName,
		parlementairesjsMpLastName,
		parlementairesjsMpNameWrapper,
		parlementairesjsMpGroup,
		parlementairesjsMpCounty,
		parlementairesjsMpGroupCountyWrapper,
		parlementairesjsMpPhone,
		parlementairesjsMpMail,
		parlementairesjsMpTwi,
		parlementairesjsMpFb,
		parlementairesjsMpInfo,
		parlementairesjsMpPhotoInfoWrapper,
		parlementairesjsMpNextWrapper,
		parlementairesjsMpNext,
		parlementairesjsCSV,
		parlementairesjsCSVWrapper,
		parlementairesjsNextMpPhoto;

	if (!this.options.datasetConfig) throw new Error("Dataset config is missing");

	var options = this.options;

	var requester = new XMLHttpRequest();
	requester.open("GET", this.options.datasetConfig);

	requester.onreadystatechange = function() {
		if (this.readyState == XMLHttpRequest.DONE) {
			if (this.status != 200) throw new Error("Cannot get dataset config (status " + this.status + ").");
			var url = options.datasetConfig;
			options.datasetConfig = JSON.parse(requester.responseText);
			options.datasetConfig.url = url;
			loadData();
		}
	};

	requester.send();

	function loadData() {
		if (!options.datasetConfig.img) throw new Error("Image options are missing");

		if (!options.datasetConfig.img.baseURL) throw new Error("Image base URL is missing");

		// If the images' base URL in dataset's config is relative, rewrite it
		if (options.datasetConfig.img.baseURL.lastIndexOf("http://", 0) !== 0 &&
			options.datasetConfig.img.baseURL.lastIndexOf("https://", 0) !== 0) {
			
			// If the dataset config is loaded from a relative URL, use the current hostname
			if (options.datasetConfig.url.lastIndexOf("http://", 0) !== 0 &&
				options.datasetConfig.url.lastIndexOf("https://", 0) !== 0) {
				options.datasetConfig.img.baseURL = window.location.protocol + "//" + window.location.host + "/" + options.datasetConfig.img.baseURL;
			} else { // If not, use the dataset config's URL
				var hostnameExtractor = document.createElement("a");
				hostnameExtractor.href = options.datasetConfig.url;
				options.datasetConfig.img.baseURL = hostnameExtractor.protocol + "//" + hostnameExtractor.host + "/" + options.datasetConfig.img.baseURL;
			}
		}

		if (!options.datasetConfig.img.width || !options.datasetConfig.img.height) throw new Error("Image size is missing");

		if (!options.datasetConfig.designation ||
			!options.datasetConfig.designation.singular ||
			!options.datasetConfig.designation.plural)
		throw new Error("Designations are missing");
		
		if (options.writeHTML      == null || options.writeHTML      == undefined) options.writeHTML      = true;
		if (options.twitter        == null || options.twitter        == undefined) options.twitter        = true;
		if (options.facebook       == null || options.facebook       == undefined) options.facebook       = true;
		if (options.download       == null || options.download       == undefined) options.download       = true;
		if (options.mpInfoDisplay  == null || options.mpInfoDisplay  == undefined) options.mpInfoDisplay  = "block";

		var requester = new XMLHttpRequest();
		requester.open("GET", options.dataset, true);

		requester.onreadystatechange = function() {
			if (this.readyState == XMLHttpRequest.DONE) {
				if (this.status != 200) throw new Error("Cannot get data (status " + this.status + ").");
				mps = JSON.parse(requester.responseText);
				showData();
			}
		};

		requester.send();

		// Load data and show fetched MPs
		function showData() {

			constructHTML();

			// Remove filtered MPs
			if (options.commissionFilter) {
				var filteredMps = [];
				for (var i=0; i<mps.length; i++) {
					if (mps[i].commissions && validateCommissionFilter(mps[i].commissions, options.commissionFilter)) {
						filteredMps.push(mps[i]);
					}
				}
				mps = filteredMps;
			}

			// List available groups and counties
			for (var i=0; i<mps.length; i++) {
				if (mps[i].county && 
					counties.indexOf(mps[i].county) == -1)
						counties.push(mps[i].county);

				if (mps[i].group                       &&
					groups.indexOf(mps[i].group) == -1 && 
					hiddenGroups.indexOf(mps[i].group) == -1) // Exclude hidden groups from the final list
						groups.push(mps[i].group);
			}

			// Remove MPs from filtered groups
			for (var i=0; i<hiddenGroups.length; i++) {
				var filteredMps = [];
				for (var i=0; i<mps.length; i++) {
					if (hiddenGroups.indexOf(mps[i].group) == -1) {
						filteredMps.push(mps[i]);
					}
				}
				mps = filteredMps;
			}

			// Fill in group select
			groups.sort();
			for (var i=0; i<groups.length; i++) {
				var groupOption = document.createElement("option");
					groupOption.value = groups[i];
					groupOption.innerHTML = groups[i];
					parlementairesjsSelectGroup.appendChild(groupOption);
			}

			// Fill in counties selector
			counties.sort();
			for (var i=0; i<counties.length; i++) {
				var countiesOption = document.createElement("option");
					countiesOption.value = counties[i];
					countiesOption.innerHTML = counties[i];
					parlementairesjsSelectCounty.appendChild(countiesOption);
			}

			// Start a first filter
			filter();
		}

		// Filter MPs according to group and/or county
		function filter() {
			// Get list of wanted MPs
			selected = [];
			for (var i=0; i<mps.length; i++) {
				if ((group  == 0 || mps[i].group  == group) &&
					(county == 0 || mps[i].county == county) &&
					validateCommissionFilter(mps[i].commissions, options.commissionFilter))
				{
					selected.push(mps[i]);
				}
			}

			// Update displayed MPs
			parlementairesjsMpTotal.innerHTML = selected.length;

			// Choose a random MP
			currentMp = Math.floor(Math.random()*selected.length);

			// Display information block if MP is available
			if (selected.length) {
				parlementairesjsMpInfo.style.display = options.mpInfoDisplay;
				next();
			} else {
				parlementairesjsMpPhoto.style.backgroundImage = "url()";
				parlementairesjsMpInfo.style.display = "none";
			}
		}

		// Select the next MP or loop
		function next() {
			currentMp++;
			if (currentMp == selected.length) currentMp = 0;
			update();
		}

		function csv() {
			var csvRawData = '"id","gender","FirstName","LastName","group","county","commissions","phone","Email","twitter","facebook"' + "\r\n";

			for (var i=0; i<mps.length; i++) {
				var rowId = (mps[i].id) ? mps[i].id : '';
				var rowGender = mps[i].gender;
				var rowFirstName = mps[i].first_name;
				var rowLastName = mps[i].last_name;
				var rowGroup = (mps[i].group) ? mps[i].group : '';
				var rowCounty = (mps[i].county) ? mps[i].county : '';
				var rowCommissions = (mps[i].commissions) ? mps[i].commissions.join(', ') : '';
				var rowPhone = (mps[i].phone) ? mps[i].phone.join(', ') : '';
				var email_candidates = []
				if (mps[i].email && options.mailFilter) {
					for (const email in mps[i].email){
						if (mps[i].email[email].split("@")[1] == options.mailFilter) {
							email_candidates.push(mps[i].email[email]);
						}
					}
				} else {
					email_candidates = mps[i].email;
				}
				var rowEmail = (email_candidates) ? email_candidates.join(',') : '';
				var rowTwitter = (mps[i].twitter) ? 'https://twitter.com/' + mps[i].twitter : '';
				var rowFacebook = (mps[i].facebook) ? 'https://www.facebook.com/' + mps[i].facebook : '';

				csvRawData += '"' + csvEscape(rowId) + '",' +
							  '"' + csvEscape(rowGender) + '",' +
							  '"' + csvEscape(rowFirstName) + '",' +
							  '"' + csvEscape(rowLastName) + '",' +
							  '"' + csvEscape(rowGroup) + '",' +
							  '"' + csvEscape(rowCounty) + '",' +
							  '"' + csvEscape(rowCommissions) + '",' +
							  '"' + csvEscape(rowPhone) + '",' +
							  '"' + csvEscape(rowEmail) + '",' +
							  '"' + csvEscape(rowTwitter) + '",' +
							  '"' + csvEscape(rowFacebook) + '"'
				csvRawData += "\r\n";
			}

			download("data.csv", "text/csv", csvRawData);
		}

		// Update MP informations
		function update()
		{
			var mp = selected[currentMp];

			resetInfo();

			parlementairesjsMpFirstName.append(mp.first_name);
			parlementairesjsMpLastName.append(mp.last_name);

			if (mp.photo) {
				if (!options.photoInImg) {
					parlementairesjsMpPhoto.style.backgroundImage = "url('"+options.datasetConfig.img.baseURL+mp.photo+".jpg')";
				} else {
					parlementairesjsMpPhoto.src = options.datasetConfig.img.baseURL+mp.photo+".jpg";
				}
			}

			var nextPhoto = (currentMp+1 == selected.length) ? selected[0].photo : selected[currentMp+1].photo;
			if (nextPhoto) parlementairesjsNextMpPhoto.src = options.datasetConfig.img.baseURL+nextPhoto+".jpg";

			if (mp.group) {
				parlementairesjsMpGroup.append(mp.group);
			}

			if (mp.county) {
				parlementairesjsMpCounty.append(mp.county);
			}

			if (mp.phone) {
				var parlementairesjsMpPhoneWrapper = document.createElement("p");
				parlementairesjsMpPhone.appendChild(parlementairesjsMpPhoneWrapper);
				for (var i=0; i<mp.phone.length; i++) {
					if (!options.phoneFilter || mp.phone[i].lastIndexOf(options.phoneFilter, 0) === 0) {
						var parlementairesjsMpPhoneChild = document.createElement("a");
						parlementairesjsMpPhoneChild.className = "parlementairesjs_mp_phone_child"
						if (mp.phone[i].charAt(0) == "+") {
							parlementairesjsMpPhoneChild.href = "tel:" + mp.phone[i];
							parlementairesjsMpPhoneChild.append(mp.phone[i]);
						} else {
							parlementairesjsMpPhoneChild.href = "tel:+" + options.datasetConfig.phone.local_prefix + mp.phone[i].substring(1).replace(/ /g, "");
							parlementairesjsMpPhoneChild.append(mp.phone[i].match(new RegExp("(.{1," + options.datasetConfig.phone.group_size + "})", "g"))
																		.join(options.datasetConfig.phone.separator));						
						}
						parlementairesjsMpPhoneWrapper.appendChild(parlementairesjsMpPhoneChild);
					}
				}
			}

			if (mp.email) {
				var parlementairesjsMpMailWrapper = document.createElement("p");
				parlementairesjsMpMail.appendChild(parlementairesjsMpMailWrapper);
				for (var i=0; i<mp.email.length; i++) {
					if (!options.mailFilter || mp.email[i].split("@")[1] == options.mailFilter) {
						var parlementairesjsMpMailChild = document.createElement("a");
						parlementairesjsMpMailChild.className = "parlementairesjs_mp_mail_child"
						parlementairesjsMpMailChild.href = "mailto:" + mp.email[i];
						
						parlementairesjsMpMailChild.append(mp.email[i]);
						parlementairesjsMpMailWrapper.appendChild(parlementairesjsMpMailChild);
					}
				}
			}

			if (options.twitter && mp.twitter) {
				var parlementairesjsMpTwiChild = document.createElement("a");
				parlementairesjsMpTwiChild.className = "parlementairesjs_mp_twi_child"
				parlementairesjsMpTwiChild.href = "https://twitter.com/" + mp.twitter;
				parlementairesjsMpTwiChild.target="_blank";
				parlementairesjsMpTwiChild.append("@"+ mp.twitter);
				parlementairesjsMpTwi.appendChild(parlementairesjsMpTwiChild);
			}

			if (options.facebook && mp.facebook) {
				var parlementairesjsMpFbChild = document.createElement("a");
				parlementairesjsMpFbChild.className = "parlementairesjs_mp_fb_child"
				parlementairesjsMpFbChild.href = "https://www.facebook.com/" + mp.facebook;
				parlementairesjsMpFbChild.target="_blank";
				parlementairesjsMpFbChild.append(mp.facebook);
				parlementairesjsMpFb.appendChild(parlementairesjsMpFbChild);
			}
		};

		function resetInfo() {
			parlementairesjsMpFirstName.innerHTML = "";
			parlementairesjsMpLastName.innerHTML = "";
			if (!options.photoInImg) {
				parlementairesjsMpPhoto.style.backgroundImage = "url()";
			} else {
				parlementairesjsMpPhoto.src = "#";
			}
			parlementairesjsMpGroup.innerHTML = "";
			parlementairesjsMpCounty.innerHTML = "";
			parlementairesjsMpPhone.innerHTML = "";
			parlementairesjsMpMail.innerHTML = "";
			parlementairesjsMpTwi.innerHTML = "";
			parlementairesjsMpFb.innerHTML = "";
		}

		function constructHTML() {
			if (options.writeHTML) {
				// parlementairesjs_introphone
				parlementairesjsMpTotal = document.createElement("span");
				parlementairesjsMpTotal.id = "parlementairesjs_mp_total";

				parlementairesjsSelectGroupDefaultOption = document.createElement("option");
				parlementairesjsSelectGroupDefaultOption.value = "0";
				parlementairesjsSelectGroupDefaultOption.append("tous les groupes");

				parlementairesjsSelectGroup = document.createElement("select");
				parlementairesjsSelectGroup.id = "parlementairesjs_select_group";
				parlementairesjsSelectGroup.appendChild(parlementairesjsSelectGroupDefaultOption);

				parlementairesjsSelectCountyDefaultOption = document.createElement("option");
				parlementairesjsSelectCountyDefaultOption.value = "0";
				parlementairesjsSelectCountyDefaultOption.append("toutes les circonscription");

				parlementairesjsSelectCounty = document.createElement("select");
				parlementairesjsSelectCounty.id = "parlementairesjs_select_county";
				parlementairesjsSelectCounty.appendChild(parlementairesjsSelectCountyDefaultOption);

				parlementairesjsIntrophone = document.createElement("p");
				parlementairesjsIntrophone.className = "parlementairesjs_introphone";
				if (options.commissionFilter) {
					parlementairesjsIntrophone.append("Au hasard parmi les ",
													parlementairesjsMpTotal,
													" ",
													options.datasetConfig.designation.plural,
													" de ",
													parlementairesjsSelectGroup,
													//document.createElement("br"),
													" et de ",
													parlementairesjsSelectCounty,
													" de la commission " + options.commissionFilter);
				} else {
					parlementairesjsIntrophone.append("Au hasard parmi les ",
													parlementairesjsMpTotal,
													" ",
													options.datasetConfig.designation.plural,
													" de ",
													parlementairesjsSelectGroup,
													document.createElement("br"),
													"et de ",
													parlementairesjsSelectCounty);
				}

				parlementairesjsIntrophoneWrapper = document.createElement("div");
				parlementairesjsIntrophoneWrapper.id = "parlementairesjs_introphone_wrapper";
				parlementairesjsIntrophoneWrapper.appendChild(parlementairesjsIntrophone);
				// !parlementairesjs_introphone

				// parlementairesjs_mp_photo
				parlementairesjsMpPhoto = (!options.photoInImg) ? document.createElement("div") : document.createElement("img");
				if (!options.photoInImg) {
					parlementairesjsMpPhoto.id = "parlementairesjs_mp_photo";
					parlementairesjsMpPhoto.style.width = options.datasetConfig.img.width;
					parlementairesjsMpPhoto.style.height = options.datasetConfig.img.height;
				} else {
					parlementairesjsMpPhoto.id = "parlementairesjs_mp_photo_img";
				}
				// !parlementairesjs_mp_photo

				// parlementairesjs_next_mp_photo
				// Create an hidden img to preload next MP's photo
				parlementairesjsNextMpPhoto = document.createElement("img");
				parlementairesjsNextMpPhoto.id = "parlementairesjs_next_mp_photo";
				parlementairesjsNextMpPhoto.style.display         = "none";
				// parlementairesjs_next_mp_photo

				// parlementairesjs_mp_photo_wrapper
				parlementairesjsMpPhotoWrapper = document.createElement("div");
				parlementairesjsMpPhotoWrapper.id = "parlementairesjs_mp_photo_wrapper";
				parlementairesjsMpPhotoWrapper.appendChild(parlementairesjsNextMpPhoto);				
				parlementairesjsMpPhotoWrapper.appendChild(parlementairesjsMpPhoto);
				// !parlementairesjs_mp_photo_wrapper

				// parlementairesjs_mp_info
				parlementairesjsMpFirstName = document.createElement("span");
				parlementairesjsMpFirstName.id = "parlementairesjs_mp_first_name";
				parlementairesjsMpLastName = document.createElement("span");
				parlementairesjsMpLastName.id = "parlementairesjs_mp_last_name";
				parlementairesjsMpNameWrapper = document.createElement("p");
				parlementairesjsMpNameWrapper.id = "parlementairesjs_mp_name_wrapper";
				parlementairesjsMpNameWrapper.append(parlementairesjsMpFirstName, " ", parlementairesjsMpLastName);

				parlementairesjsMpGroup = document.createElement("p");
				parlementairesjsMpGroup.id = "parlementairesjs_mp_group";
				parlementairesjsMpCounty = document.createElement("p");
				parlementairesjsMpCounty.id = "parlementairesjs_mp_county";

				parlementairesjsMpPhone = document.createElement("div");
				parlementairesjsMpPhone.id = "parlementairesjs_mp_phone";

				parlementairesjsMpMail = document.createElement("div");
				parlementairesjsMpMail.id = "parlementairesjs_mp_mail";

				parlementairesjsMpTwi = document.createElement("p");
				parlementairesjsMpTwi.id = "parlementairesjs_mp_twi";

				parlementairesjsMpFb = document.createElement("p");
				parlementairesjsMpFb.id = "parlementairesjs_mp_fb";

				parlementairesjsMpInfo = document.createElement("div");
				parlementairesjsMpInfo.id = "parlementairesjs_mp_info";
				parlementairesjsMpInfo.append(parlementairesjsMpNameWrapper, parlementairesjsMpGroup, parlementairesjsMpCounty, parlementairesjsMpPhone, parlementairesjsMpMail, parlementairesjsMpTwi, parlementairesjsMpFb);
				// !parlementairesjs_mp_info

				// parlementairesjs_mp_photo_info_wrapper
				parlementairesjsMpPhotoInfoWrapper = document.createElement("div");
				parlementairesjsMpPhotoInfoWrapper.id = "parlementairesjs_mp_photo_info_wrapper";
				parlementairesjsMpPhotoInfoWrapper.append(parlementairesjsMpPhotoWrapper, parlementairesjsMpInfo);
				// !parlementairesjs_mp_photo_info_wrapper

				// parlementairesjs_mp_next
				parlementairesjsMpNext = document.createElement("p");
				parlementairesjsMpNext.id = "parlementairesjs_mp_next";
				parlementairesjsMpNext.append(options.datasetConfig.designation.singular.charAt(0).toUpperCase()+options.datasetConfig.designation.singular.slice(1), " suivant·e >");
				parlementairesjsMpNextWrapper = document.createElement("div");
				parlementairesjsMpNextWrapper.id = "parlementairesjs_mp_next_wrapper";
				parlementairesjsMpNextWrapper.appendChild(parlementairesjsMpNext);

				target.append(parlementairesjsIntrophoneWrapper, parlementairesjsMpPhotoInfoWrapper, parlementairesjsMpNextWrapper);

				// parlementairesjs_csv
				if (options.download) {
					parlementairesjsCSV = document.createElement("p");
					parlementairesjsCSV.id = "parlementairesjs_csv";
					parlementairesjsCSV.append("Télécharger les données au format CSV");
					parlementairesjsCSVWrapper = document.createElement("div");
					parlementairesjsCSVWrapper.id = "parlementairesjs_csv_wrapper";
					parlementairesjsCSVWrapper.appendChild(parlementairesjsCSV);
					target.append(parlementairesjsCSVWrapper);
					parlementairesjsCSV.onclick = csv;
				}
			} else {
				parlementairesjsMpTotal = document.getElementById("parlementairesjs_mp_total");
				parlementairesjsSelectGroup = document.getElementById("parlementairesjs_select_group");
				parlementairesjsSelectCounty = document.getElementById("parlementairesjs_select_county");
				parlementairesjsMpPhoto = (!options.photoInImg) ? document.getElementById("parlementairesjs_mp_photo") : document.getElementById("parlementairesjs_mp_photo_img");
				parlementairesjsMpFirstName = document.getElementById("parlementairesjs_mp_first_name");
				parlementairesjsMpLastName = document.getElementById("parlementairesjs_mp_last_name");
				parlementairesjsMpGroup = document.getElementById("parlementairesjs_mp_group");
				parlementairesjsMpCounty = document.getElementById("parlementairesjs_mp_county");
				parlementairesjsMpPhone = document.getElementById("parlementairesjs_mp_phone");
				parlementairesjsMpMail = document.getElementById("parlementairesjs_mp_mail");
				parlementairesjsMpTwi = document.getElementById("parlementairesjs_mp_twi");
				parlementairesjsMpFb = document.getElementById("parlementairesjs_mp_fb");
				parlementairesjsMpInfo = document.getElementById("parlementairesjs_mp_info");
				parlementairesjsMpNext = document.getElementById("parlementairesjs_mp_next");
			}

			parlementairesjsMpNext.onclick = next;

			// Set filter event when a group is selected
			parlementairesjsSelectGroup.onchange = function() {
				group = this.value;
				filter();
			}

			// Set filter event when a group is selected
			parlementairesjsSelectCounty.onchange = function() {
				county = this.value;
				filter();
			}
		}

	};
};

function validateCommissionFilter(commissions, commissionFilter) {
	if (!commissionFilter) return true;

	for (var i=0; i<commissions.length; i++) {
		if (commissions[i] == commissionFilter) return true;
	}

	return false;
};

function download(filename, mime, text) {
	var element = document.createElement('a');
	element.setAttribute('href', 'data:' + mime + ';charset=utf-8,' + encodeURIComponent(text));
	element.setAttribute('download', filename);

	element.style.display = 'none';
	document.body.appendChild(element);

	element.click();

	document.body.removeChild(element);
}

function csvEscape(string) {
	return new String(string).replace(/\"/g, "\\\"");
}
