#!/usr/bin/env bash

set -e

function trap_errors() {
  echo ""
  echo "Debbuging:"
  echo "  pwd:      $(pwd)"
  echo "  MAIN_DIR: ${MAIN_DIR}"
  clean_on_exit
}

function clean_on_exit() {
  echo -n "Cleaning..."
  if [ ! -z "${TMP_DIR}" ]; then
    rm -rf "${TMP_DIR}"
  fi
  echo " done."
}

trap trap_errors ERR

MAIN_DIR=$(realpath "$(dirname $0)/../../")

USER_AGENT="Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/109.0"

cd "${MAIN_DIR}"

echo "WARNING WARNING WARNING!"
echo "This script extracts phone numbers from MP's website, but it cannot check if"
echo "those numbers do belong to them. Please check *each* phone number before"
echo "adding it to the database!"
echo ""

TMP_DIR=$(mktemp -d)
mkdir -p "${TMP_DIR}/data/"

echo -n "Downloading data..."
wget -q -U "${USER_AGENT}" "https://data.assemblee-nationale.fr/static/openData/repository/16/amo/deputes_actifs_mandats_actifs_organes/AMO10_deputes_actifs_mandats_actifs_organes.json.zip" -O "${TMP_DIR}/data/an.zip"
cd "${TMP_DIR}/data/"
echo " done."

echo -n "Extracting data..."
unzip an.zip > /dev/null
echo " done."
echo ""

find json/acteur/ -type f | sed 's/\.json//i' | sed 's/json\/acteur\///i' | while read -r key; do
  first_name=$(jq -r .acteur.etatCivil.ident.prenom "json/acteur/${key}.json")
  last_name=$(jq -r .acteur.etatCivil.ident.nom "json/acteur/${key}.json")

  websites=$(jq -r '.acteur.adresses.adresse | map(. | select(.type=="22")) | .[].valElec' "json/acteur/${key}.json" | awk '{print tolower($0)}' | sed 's/\/$//i')

  phoneRaw=$(jq -r '.acteur.adresses.adresse | map(. | select(.type=="11")) | .[].valElec' "json/acteur/${key}.json" | tac)
  phone=""
  if [ ! -z "$phoneRaw" ]; then
    for i in $phoneRaw; do
      phone="$(echo ${i} | tr -d ' .' | tr -d ' -' | sed 's/(0)//i' | sed 's/^00/\+/i' | sed 's/^0590/\+590/i' | sed 's/^0596/\+596/i' | sed 's/^0594/\+594/i' | sed 's/^0262/\+262/i' | sed 's/^0508/\+508/i' | sed 's/^0269/\+262269/i' )"$'\n'"${phone}"
    done
  fi

  phonesChamber=$(jq -r "map(select(.name==\"${first_name} ${last_name}\")) | .[].phone" "${MAIN_DIR}/misc/an/phones.json" | tr -d ' .')

  if [ ! -z "$phonesChamber" ]; then
    for i in $phonesChamber; do
      phone=$(echo "${phone}" | sed "s/${i}//g" | sort -u)
      phone="${i}"$'\n'"${phone}"
    done
  fi

  if [ ! -z "${websites}" ]; then
    for i in ${websites}; do
      echo "Found website for ${first_name} ${last_name}: ${i}"
      WEBSITE_TMP_DIR=$(mktemp -d)
      cd "${WEBSITE_TMP_DIR}"
      wget -q -U "${USER_AGENT}" --mirror --no-parent -l 1 -e robots=off --random-wait -E --follow-tags=a -T 5 -t 1 "https://${i}/" || 
      wget -q -U "${USER_AGENT}" --mirror --no-parent -l 1 -e robots=off --random-wait -E --follow-tags=a -T 5 -t 1 "http://${i}/"  ||
      true
      if [ -z "$(ls -A "${WEBSITE_TMP_DIR}")" ]; then
        echo "  Cannot scrap website."
      else
        first=""
        phoneNumbers=$(find . -type f -name \*.html -exec grep -Po '(?<=(tel\:)| |\>)(((\+|00)33[0-9]{9})|(0[0-9]{9})|0[0-9](\.|-| )([0-9]{2}(\.|-| )){3}[0-9]{2})' {} \; | tr -d ' .' | tr -d ' -' | sed 's/(0)//i' | sed 's/^00/\+/i' | sed 's/^0590/\+590/i' | sed 's/^0596/\+596/i' | sed 's/^0594/\+594/i' | sed 's/^0262/\+262/i' | sed 's/^0508/\+508/i' | sed 's/^0269/\+262269/i' | sort -u)
        if [ ! -z "$phoneNumbers" ]; then
          for i in $phoneNumbers; do
            newNumbers=""
            if ! (grep -q "${i}" <<< "${phone}"); then
              if [ -z "$first" ]; then
                echo "  New phone numbers found:"
                first=1
              fi
              echo "    - ${i}"
            fi
          done
        fi
      fi
      cd "${TMP_DIR}/data/"
      rm -rf "${WEBSITE_TMP_DIR}"
      echo ""
    done
  fi
done

clean_on_exit
