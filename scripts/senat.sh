#!/usr/bin/env bash

set -e

function trap_errors() {
  echo ""
  echo "Debbuging:"
  echo "  pwd:      $(pwd)"
  echo "  MAIN_DIR: ${MAIN_DIR}"
  clean_on_exit
}

function clean_on_exit() {
  echo -n "Cleaning..."
  if [ ! -z "${TMP_DIR}" ]; then
    pg_ctl -D "${TMP_DIR}/postgres/" -o "-c unix_socket_directories='${TMP_DIR}/postgres-tmp/'" stop 2>/dev/null || true
    rm -rf "${TMP_DIR}"
  fi
  echo " done."
}

trap trap_errors ERR

MAIN_DIR=$(realpath "$(dirname $0)/../")

USER_AGENT="Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/109.0"

cd "${MAIN_DIR}"

echo -n "Creating directories..."
TMP_DIR=$(mktemp -d)
mkdir -p "${TMP_DIR}/data/"
mkdir -p "${TMP_DIR}/postgres/"
mkdir -p "${TMP_DIR}/postgres-tmp/"
mkdir -p "${MAIN_DIR}/data/gen/senat/images/"
echo " done."

echo -n "Configuring and starting postgres..."
initdb --auth=trust --username=postgres -D "${TMP_DIR}/postgres/" > /dev/null

pg_ctl -D "${TMP_DIR}/postgres" -o "-c unix_socket_directories='${TMP_DIR}/postgres-tmp/'" start > /dev/null

psql --host=127.0.0.1 --port=5432 --username=postgres --no-password postgres -c "CREATE USER opendata WITH SUPERUSER;" > /dev/null
psql --host=127.0.0.1 --port=5432 --username=postgres --no-password postgres -c "CREATE DATABASE opendata OWNER opendata;" > /dev/null
echo " done."

echo -n "Downloading data..."
wget -q -U "${USER_AGENT}" "https://data.senat.fr/data/senateurs/export_sens.zip" -O "${TMP_DIR}/data/senat.zip"
cd "${TMP_DIR}/data/"
echo " done."

echo -n "Extracting data..."
unzip senat.zip > /dev/null

psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata < "${TMP_DIR}/data/export_sens.sql" > /dev/null

psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata -c "\t" -c "\a" -c "\o ${TMP_DIR}/data/sen.json" -c "SELECT row_to_json(r) FROM sen AS r WHERE etasencod = 'ACTIF';" > /dev/null
psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata -c "\t" -c "\a" -c "\o ${TMP_DIR}/data/senurl.json" -c "SELECT row_to_json(r) FROM senurl AS r;" > /dev/null
psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata -c "\t" -c "\a" -c "\o ${TMP_DIR}/data/com.json" -c "SELECT row_to_json(r) FROM com AS r;" > /dev/null
psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata -c "\t" -c "\a" -c "\o ${TMP_DIR}/data/memcom.json" -c "SELECT row_to_json(r) FROM memcom AS r;" > /dev/null
psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata -c "\t" -c "\a" -c "\o ${TMP_DIR}/data/grppol.json" -c "SELECT row_to_json(r) FROM grppol AS r;" > /dev/null
psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata -c "\t" -c "\a" -c "\o ${TMP_DIR}/data/telephone.json" -c "SELECT row_to_json(r) FROM telephone AS r;" > /dev/null
psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata -c "\t" -c "\a" -c "\o ${TMP_DIR}/data/mel.json" -c "SELECT row_to_json(r) FROM mel AS r;" > /dev/null
psql --host=127.0.0.1 --port=5432 --username=postgres --no-password opendata -c "\t" -c "\a" -c "\o ${TMP_DIR}/data/poicon.json" -c "SELECT row_to_json(r) FROM poicon AS r;" > /dev/null
echo " done."

echo -n "Stopping postgres..."
pg_ctl -D "${TMP_DIR}/postgres/" -o "-c unix_socket_directories='${TMP_DIR}/postgres-tmp/'" stop > /dev/null
echo " done."

for row in $(cat ${TMP_DIR}/data/sen.json | jq -r '. | @base64'); do
  _jq() {
    echo ${row} | base64 --decode | jq -r ${1}
  }

  IFS=$'\n'
  id=$(_jq '.senmat')
  echo -n "Parsing ${id}"
  last_name=$(_jq '.sennomuse')
  first_name=$(_jq '.senprenomuse')
  echo " (${first_name} ${last_name})..."
  groupRef=$(_jq '.sengrppolliccou')
  group=$(jq -r "select(.grppolliccou==\"${groupRef}\") | .grppollilcou" ${TMP_DIR}/data/grppol.json)
  county=$(_jq '.sencircou')

  poiconId=$(jq -r "select(.senmat==\"${id}\") | .poiconid" ${TMP_DIR}/data/poicon.json | sort -u)

  commissionsRef=$(jq -r "select(.senmat==\"${id}\" and .temvalcod==\"ACTIF\" and .memcomtitsup!=\"SUPPLEANT\") | .orgcod" ${TMP_DIR}/data/memcom.json | sort -u)
  commissions=""
  if [ ! -z "$commissionsRef" ]; then
    for i in $commissionsRef; do
      comNom=$(jq -r "select(.orgcod==\"${i}\" and (.typorgcod==\"COM\" or .typorgcod==\"COMEUR\") and .orgcod!=\"NEANT\") | .evelib" ${TMP_DIR}/data/com.json)
      if [ ! -z "$comNom" ]; then
        commissions="${commissions}"$'\n'"${comNom}"
      fi
    done
  fi

  civ=$(_jq '.quacod')
  if [ "$civ" == "M." ]; then
    gender="m"
  else
    gender="f"
  fi

  email=$(_jq '.senema' | sed 's/null//i')
  phone=""

  if [ ! -z "$poiconId" ]; then
    for i in $poiconId; do
      phone="${phone}"$'\n'"$(jq -r "select(.poiconid==\"${i}\" and .telnum!=null) | .telnum" ${TMP_DIR}/data/telephone.json)"
      email="${email}"$'\n'"$(jq -r "select(.poiconid==\"${i}\" and .melema!=null) | .melema" ${TMP_DIR}/data/mel.json)"
    done
  fi

  twitter=$(jq -r "select((.senmat==\"${id}\") and (.typurlcod==\"TWEETER\") and (.temvalcod==\"ACTIF\")) | .senurlurl" ${TMP_DIR}/data/senurl.json | head -1 | sed 's/http:\/\/twitter\.com//i' | sed 's/https:\/\/twitter\.com//i' | sed 's/\/#!\///ig' | sed 's/\?lang=fr//i' | sed 's/https:\/tw\.unblock-us\.com\///i' | sed 's/\///ig')
  facebook=$(jq -r "select((.senmat==\"${id}\") and (.typurlcod==\"FACEBOOK\") and (.temvalcod==\"ACTIF\")) | .senurlurl" ${TMP_DIR}/data/senurl.json | head -1 | sed 's/https:\/\/fr-fr.facebook.com\///i' | sed 's/https:\/\/www.facebook.com\///i' | sed 's/http:\/\/fr-fr.facebook.com\///i' | sed 's/http:\/\/www.facebook.com\///i' | sed 's/home\.php?#!\///ig'  | sed 's/home\.php?#\///ig' | sed 's/?ref=profile//ig' | sed 's/?ref=ts//ig' | sed 's/&ref=profile//ig' | sed 's/&ref=ts//ig' | sed 's/?sk=wall//ig' | sed 's/&sk=wall//ig')
  public_id=$(echo "${last_name}_${first_name}${id}" | iconv -f utf8 -t ascii//TRANSLIT | awk '{print tolower($0)}' | sed 's/-/_/ig' | sed 's/ /_/ig')

  echo -n "  Writing data..."

  filename="${TMP_DIR}/data/${id}.yml"

  echo "id: ${id}" > "${filename}"
  echo "last_name: ${last_name}" >> "${filename}"
  echo "first_name: ${first_name}" >> "${filename}"
  echo "group: ${group}" >> "${filename}"
  echo "county: ${county}" >> "${filename}"
  echo "gender: ${gender}" >> "${filename}"

  echo "commissions:" >> "${filename}"
  if [ ! -z "${commissions}" ]; then
    for i in ${commissions}; do
      echo "- \"${i}\"" >> "${filename}"
    done
  fi

  echo "phone:" >> "${filename}"
  if [ ! -z "${phone}" ]; then
    for i in ${phone}; do
      echo "- \"${i}\"" >> "${filename}"
    done
  fi

  echo "email:" >> "${filename}"
  if [ ! -z "${email}" ]; then
    for i in ${email}; do
      echo "- \"${i}\"" >> "${filename}"
    done
  fi

  echo "twitter: \"${twitter}\"" >> "${filename}"
  echo "facebook: \"${facebook}\"" >> "${filename}"
  echo "photo: ${public_id}" >> "${filename}"

  echo " done."

  echo -n "  Downloading photo..."
  if [ ! -f "${MAIN_DIR}/data/gen/senat/images/${public_id}.jpg" ]; then
    photo=$(curl --silent "https://www.senat.fr/senateur/${public_id}.html" | sed -n '/<img/s/.*src=\([^ ]*\).*/\1/p' | sed 's/\"//g' | grep -F "/senimg/" | head -1)
    wget -q -U "${USER_AGENT}" "http://www.senat.fr${photo}" -O "${MAIN_DIR}/data/gen/senat/images/${public_id}.jpg"
    magick "${MAIN_DIR}/data/gen/senat/images/${public_id}.jpg" -resize 155x155 "${MAIN_DIR}/data/gen/senat/images/${public_id}.jpg"
  fi
  echo " done."
done

yq -c -s . "${TMP_DIR}/data/"*.yml > "${MAIN_DIR}/data/gen/senat/dataset.json"
cp -L "${MAIN_DIR}/data/senat.json" "${MAIN_DIR}/data/gen/senat/config.json"

clean_on_exit
