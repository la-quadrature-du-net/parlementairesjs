# ParlementairesJS - Display information about MPs and engage your audience to contact them

ParlementairesJS displays a simple frame to show phone numbers, email addresses, Twitter accounts and Facebook links about MPs. Filters can be configured to match campaign's requirements.

Demo: https://parlementairesjs.eu.org/demo.html

## Usage

### Javascript (recommended)

1. In the `<head>` section, you must include the script and its CSS:

```html
<script src="https://parlementairesjs.eu.org/parlementaires.js"></script>
<link rel='stylesheet' href='https://parlementairesjs.eu.org/parlementaires.css' type='text/css' media='all' />
```

You can download a copy of the `parlementaires.js` and `parlementaires.css` and load your local copy.

2. You need to add a `<div>` element in your web page, where the frame will be displayed. This `<div>` must have an ID.

3. Add the following code at the end of your web page, before the `</body>`:

```html
<script>
    ParlementairesJS({
        dataset: "https://parlementairesjs.eu.org/data/gen/an/dataset.json",
        datasetConfig: "https://parlementairesjs.eu.org/data/gen/an/config.json"
    }).display('id-of-your-div');
</script>
```

You must set at least the `dataset` and `datasetConfig` values. Change the `id-of-your-div` with the ID of your `<div>` you added in stage 2. URIs don't require a FQDN (useful if you host the data/config yourself).

### Options

| Option             | Mandatory? | Description                                                                               | Default               |
| ------------------ | ---------- | ----------------------------------------------------------------------------------------- | --------------------- |
| `dataset`          | true       | URL of the MPs' dataset.                                                                  | N/A                   |
| `datasetConfig`    | true       | URL of the dataset's configuration.                                                       | N/A                   |
| `commissionFilter` | false      | Display MPs that are in the specified commission.                                         | No filter is applied. |
| `writeHTML`        | false      | Write the HTML in the `<div>`. Disable this if you want to change the current stylesheet. | `true`                |
| `photoInImg`       | false      | Put the image in a `<img>` instead of in a `<div>`.                                       | `false`               |
| `phoneFilter`      | false      | Display phone numbers that start with this value.                                         | No filter is applied. |
| `mailFilter`       | false      | Display emails with the specified domain name.                                            | No filter is applied. |
| `twitter`          | false      | Display Twitter link.                                                                     | `true`                |
| `facebook`         | false      | Display Facebook link.                                                                    | `true`                |
| `download`         | false      | Display a link to download data as a CSV file.                                            | `true`                |
| `hiddenGroups`     | false      | Do not display MPs in on of the specified parliamentary group.                            | `[]`                  |
| `mpInfoDisplay`    | false      | Style of the `parlementairesjs_mp_info` element.                                          | `"block"`             |

## Iframe (fallback)

If you are unable to add a script to your web page, you can fallback to the iframe method. Please note that you cannot have a custom CSS nor options by doing this. Datasets available are limited to those [listed bellow](#datasets-and-their-configuration).

Without a filter:

```html
<iframe src="https://parlementairesjs.eu.org/iframe.html#dataset" frameborder="1" width="100%" height="640px">
```

With a filter:

```html
<iframe src="https://parlementairesjs.eu.org/iframe.html#dataset-filter" frameborder="1" width="100%" height="640px">
```

## Generated HTML

The following HTML is generated:

```html
<div id="id-of-your-div">
    <div id="parlementairesjs_introphone_wrapper">
        <p class="parlementairesjs_introphone">
            Au hasard parmi les <span id="parlementairesjs_mp_total">576</span> député·es de
            <select id="parlementairesjs_select_group">
                <option value="0">tous les groupes</option>
            </select>
            <br> et de
            <select id="parlementairesjs_select_county">
                <option value="0">toutes les circonscription</option>
            </select>
        </p>
    </div>
    <div id="parlementairesjs_mp_photo_info_wrapper">
        <div id="parlementairesjs_mp_photo_wrapper">
            <div id="parlementairesjs_mp_photo" style="width: 150px; height: 192px; background-image: url('http://localhost:8000//images/an/334116.jpg');"></div>
        </div>
        <div id="parlementairesjs_mp_info" style="display: block;">
            <p id="parlementairesjs_mp_name_wrapper">
                <span id="parlementairesjs_mp_first_name">John</span> <span id="parlementairesjs_mp_last_name">Doe</span>
            </p>
            <p id="parlementairesjs_mp_group">Some Group</p>
            <p id="parlementairesjs_mp_county">Some county</p>
            </p>
            <div id="parlementairesjs_mp_phone">
                <p>
                    <a class="parlementairesjs_mp_phone_child" href="tel:+33123456789">01 23 45 67 89</a>
                    <a class="parlementairesjs_mp_phone_child" href="tel:+33600000000">+33600000000</a>
                </p>
            </div>
            <div id="parlementairesjs_mp_mail">
                <p>
                    <a class="parlementairesjs_mp_mail_child" href="mailto:john.doe@example.org">john.doe@example.org</a>
                    <a class="parlementairesjs_mp_mail_child" href="mailto:john.doe2@example.org">john.doe2@example.org</a>
                </p>
            </div>
            <p id="parlementairesjs_mp_twi">
                <a class="parlementairesjs_mp_twi_child" href="https://twitter.com/SommeTwitterAccount" target="_blank">@SommeTwitterAccount</a>
            </p>
            <p id="parlementairesjs_mp_fb">
                <a class="parlementairesjs_mp_fb_child" href="https://www.facebook.com/SomeFacebookLink" target="_blank">SomeFacebookLink</a>
            </p>
        </div>
    </div>
    <div id="parlementairesjs_mp_next_wrapper">
        <p id="parlementairesjs_mp_next">Député·e suivant·e &gt;</p>
    </div>
    <div id="parlementairesjs_csv_wrapper">
        <p id="parlementairesjs_csv">Télécharger les données au format CSV</p>
    </div>
</div>
```

You can override CSS style using those HTML classes and IDs.

If you set `writeHTML` to `false`, your HTML must have the following elements:

- `parlementairesjs_mp_total`
- `parlementairesjs_select_group`
- `parlementairesjs_mp_photo` (if `photoToImg` is `false`)
- `parlementairesjs_mp_photo_img` (if `photoToImg` is `true`)
- `parlementairesjs_mp_first_name`
- `parlementairesjs_mp_last_name`
- `parlementairesjs_mp_group`
- `parlementairesjs_mp_county`
- `parlementairesjs_mp_phone`
- `parlementairesjs_mp_mail`
- `parlementairesjs_mp_twi`
- `parlementairesjs_mp_fb`
- `parlementairesjs_mp_info`
- `parlementairesjs_mp_next`
- `parlementairesjs_csv` (if `download` is `true`)

## Datasets and their configuration

Datasets must be shipped with their configuration. Datasets contain MPs, configurations contain information about formatting.

Available datasets:

| Name                         | ID      | Description               | Data's source                                                                                |
| ---------------------------- | ------- | ------------------------- | -------------------------------------------------------------------------------------------- |
| Assemblée nationale (France) | `an`    | French national assembly. | Chamber's open data + additional listing for phone numbers (see `scripts/an.sh` for details) |
| Sénat (France)               | `senat` | French Senate.            | Chamber's open data (see `scripts/senat.sh` for details)                                     |

Datasets are available at `https://parlementairesjs.eu.org/data/gen/<dataset>/dataset.json` and their configuration at `https://parlementairesjs.eu.org/data/gen/<dataset>/config.json`.

You can generate a dataset with `make <dataset-id>`.

## Create your dataset

Generated datasets are located at `data/gen/<dataset>/dataset.json` and their configuration is at `data/gen/<dataset>/config.json`. Each dataset must be generated by a script located at `scripts/<dataset>.sh`. If you need to have other data in the git tree to include in the generation process, put it in the `misc/<dataset>/` folder.

Generated datasets are a JSON array like this:

```json
[
  {
    "id": "XXXXXX",
    "last_name": "John",
    "first_name": "Doe",
    "group": "Some group",
    "county": "Some county",
    "commissions": [
      "A first commission",
      "Another commission"
    ],
    "phone": [
      "0123456789",
      "+33600000000"
    ],
    "email": [
      "john.doe@example.org",
      "john.doe2@example.org"
    ],
    "twitter": "SommeTwitterAccount",
    "facebook": "SomeFacebookLink",
    "photo": "XXXXXX"
  },
  {
    "id": "YYYYYY",
    "last_name": "John",
    "first_name": "Doe",
    "group": "Some group",
    "county": "Some county",
    "commissions": [
      "A first commission",
      "Another commission"
    ],
    "phone": [
      "0123456789",
      "+33600000000"
    ],
    "email": [
      "john.doe@example.org",
      "john.doe2@example.org"
    ],
    "twitter": "SommeTwitterAccount",
    "facebook": "SomeFacebookLink",
    "photo": "YYYYYY"
  }
]
```

Mandatory fields are `last_name`, `first_name`.

Configuration files are located at `/data/<dataset>.json` (and copied to `/data/gen/<dataset>/config.json` when generating data) and must follow that format:

```json
{
  "designation": {
    "singular": "député·e",
    "plural": "député·es"
  },
  "phone": {
    "local_prefix": "33",
    "separator": " ",
    "group_size": 2
  },
  "img": {
    "baseURL": "/data/gen/an/images/",
    "width": "150px",
    "height": "192px"
  }
}
```

All fields are mandatory. `baseURL` can be a relative URL or with a FQDN. For privacy issues, you should get a copy of MPs' photos in `/data/gen/<dataset>/images/` and use a relative `baseURL`.

## License

Source code is MIT licensed. Data may have a different license.
